#!/bin/bash
set -e

if [[ -z "$1" ]]; then
  echo "error: version number required."
  echo "$0 version_number"
  exit 1
fi

VERSION_NUMBER="$1"
PACKAGE_NAME="snake-$VERSION_NUMBER"
EXEC_NAME="Snake"

ASSETS="assets"
IMAGES="$ASSETS/images"
SOUNDS="$ASSETS/sounds"
SNAKE="snake"
MAIN="$SNAKE/main.py"
LICENSE="LICENSE"
README="README.md"

# Check if a file exists in pwd.
# $1 the file to check for
function check_file {
  if [[ -f "$1" ]]; then
    echo "found file: $1"
  else
    echo "error: could not find file: $1"
    exit 1
  fi
}

# Check if the directory exists in pwd.
# $1 the directory to check for
function check_dir {
  if [[ -d "$1" ]]; then
    echo "found directory: $1"
  else
    echo "error: could not find directory: $1"
    exit 1
  fi
}

echo "Verifying project structure..."
check_dir "$ASSETS"
check_dir "$IMAGES"
check_dir "$SOUNDS"
check_dir "$SNAKE"
check_file "$MAIN"
check_file "$LICENSE"
check_file "$README"
echo "Done."

echo "Creating package structure..."
mkdir -pv "$PACKAGE_NAME"
mkdir -pv "$PACKAGE_NAME/$SNAKE"
mkdir -pv "$PACKAGE_NAME/$IMAGES"
mkdir -pv "$PACKAGE_NAME/$SOUNDS"
echo "Done."

echo "Copying files to the package..."
cp -rv "$IMAGES"/* "$PACKAGE_NAME/$IMAGES"
cp -rv "$SOUNDS"/* "$PACKAGE_NAME/$SOUNDS"
cp -rv "$SNAKE"/*.py "$PACKAGE_NAME/$SNAKE"
cp -v "$LICENSE" "$PACKAGE_NAME/$LICENSE"
cp -v "$README" "$PACKAGE_NAME/$README"
echo "Done."

echo "Entering $PACKAGE_NAME"
cd "$PACKAGE_NAME"
echo "Linking..."
ln -sfv "$MAIN" "$EXEC_NAME"
cd -
echo "Exited $PACKAGE_NAME"

echo "Creating archive file..."
tar -vczf "$PACKAGE_NAME.tar.gz" "$PACKAGE_NAME"
echo "Done."

echo "Cleaning..."
rm -rfv "$PACKAGE_NAME"
mv -v "$PACKAGE_NAME.tar.gz" "dist"
echo "Done."
