# Copyright (c) 2019, Sciancisco
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyrigth holder nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""UI part of the snake game."""

import os
import pygame
from paths import IMAGES, SOUNDS
from snake_model import Snake


class SnakeGUI:
    """Graphical representation of the Snake model."""

    # Control keys: normie, gamer, Vim user
    UP_KEYS = (pygame.K_UP, pygame.K_w, pygame.K_k)
    DOWN_KEYS = (pygame.K_DOWN, pygame.K_s, pygame.K_j)
    LEFT_KEYS = (pygame.K_LEFT, pygame.K_a, pygame.K_h)
    RIGHT_KEYS = (pygame.K_RIGHT, pygame.K_d, pygame.K_l)

    def __init__(self, snake):
        """
        Instantiate a SnakeUI.

        Most of the settings are here.

        Parameters
        ----------
        snake: snake.Snake
            the model of the game

        """
        self.snake = snake  # The model

        # Visual assets settings
        pygame.display.init()
        pygame.display.set_mode((1, 1))  # Only there so pygame can load images
        icon = None  # Placeholder for the icon of the game
        try:  # to load the images
            grass_tile = pygame.image.load(
                os.path.join(IMAGES, "grass.png")
            ).convert_alpha()
            self.snake_head_tile = pygame.image.load(
                os.path.join(IMAGES, "snake_head.png")
            ).convert_alpha()
            self.snake_body_tile = pygame.image.load(
                os.path.join(IMAGES, "snake_body.png")
            ).convert_alpha()
            self.snake_tail_tile = pygame.image.load(
                os.path.join(IMAGES, "snake_tail.png")
            )
            self.apple_tile = pygame.image.load(
                os.path.join(IMAGES, "apple.png")
            ).convert_alpha()
            icon = pygame.image.load(  # Load the icon
                os.path.join(IMAGES, "icon.png")
            )

            self.tile_w = grass_tile.get_width()
            self.tile_h = grass_tile.get_height()
            self.background = pygame.Surface((  # The background of the game
                self.snake.xs * self.tile_w,
                self.snake.ys * self.tile_h
            ))
            for x in range(self.snake.xs):  # blit the grass tile all over
                for y in range(self.snake.ys):
                    self.background.blit(
                        grass_tile,
                        (x * self.tile_w, y * self.tile_h)
                    )

        except (pygame.error, FileNotFoundError) as e:
            # a failure, fall back to simple graphics
            print("{}. Snake will use simple graphics.".format(e))

            self.tile_w = 20  # Width of a tile
            self.tile_h = 20  # Height of a tile
            self.snake_body_tile = pygame.Surface((self.tile_w, self.tile_h))
            self.snake_body_tile.fill((255, 255, 255))  # Color of the snake
            self.snake_head_tile = self.snake_tail_tile = self.snake_body_tile
            self.apple_tile = pygame.Surface((self.tile_w, self.tile_h))
            self.apple_tile.fill((255, 0, 0))  # Color of the apple tile
            self.background = pygame.Surface((
                self.snake.xs * self.tile_w,
                self.snake.ys * self.tile_h
            ))
            self.background.fill((0, 0, 0))
            icon = pygame.Surface((32, 32))  # Simple icon
            icon.fill((0, 0, 0))
            icon.blit(self.apple_tile, (16, 1))
            icon.blit(self.snake_head_tile, (1, 16))

        # Audio assets settings
        pygame.mixer.init()
        try:  # to load the sound
            self.gulp_sound = pygame.mixer.Sound(  # When the snake eats
                os.path.join(SOUNDS, "gulp.ogg")
            )
            self.game_over_sound = pygame.mixer.Sound(  # When game ends
                os.path.join(SOUNDS, "game_over.ogg")
            )
            self.loop_sound = pygame.mixer.Sound(
                os.path.join(SOUNDS, "loop.ogg")
            )
            self.gulp_sound.set_volume(1.0)
            self.game_over_sound.set_volume(0.2)
            self.loop_sound.set_volume(0.1)
        except (pygame.error, FileNotFoundError) as e:  # a failure
            print("{}. Snake will be silent.".format(e))
            pygame.mixer.quit()
            fake_sound = lambda loops=0: None  # Is it sketch? Totally!
            fake_sound.play = fake_sound  # Does it work? Absolutely!
            fake_sound.stop = fake_sound  # Is it suprising? yeah...
            self.gulp_sound = fake_sound
            self.game_over_sound = self.loop_sound = self.gulp_sound

        # Text settings
        pygame.font.init()
        self.font = pygame.font.Font(None, 30)  # Load default pygame font
        self.text_color = (255, 255, 255)  # Color for the text
        self.score_str = "Score: {}"

        # Display settings
        self.display = pygame.display.set_mode((
            self.background.get_width(),
            self.background.get_height()
            ))
        pygame.display.set_caption("Snake")
        pygame.display.set_icon(icon)

    def get_input(self, vec):
        """
        Get the inputs from the player.

        Parameters
        ----------
        vec: tuple or list
            last direction of the snake
        Return
        ------
        current direction of the player

        """
        events = pygame.event.get(pygame.KEYDOWN)
        pygame.event.clear()
        for e in events:
            if e.key in self.UP_KEYS:
                return Snake.UP if vec != Snake.DOWN else vec
            elif e.key in self.DOWN_KEYS:
                return Snake.DOWN if vec != Snake.UP else vec
            elif e.key in self.LEFT_KEYS:
                return Snake.LEFT if vec != Snake.RIGHT else vec
            elif e.key in self.RIGHT_KEYS:
                return Snake.RIGHT if vec != Snake.LEFT else vec
        return vec

    def print_display(self):
        """Print the current state of the game on the display."""
        # Hide previous frame
        self.display.blit(self.background, (0, 0))
        # Print the apple
        self.display.blit(self.apple_tile, (
            self.snake.apple[0] * self.tile_w,
            self.snake.apple[1] * self.tile_h
        ))
        # Print the snake
        s = self.snake.snake[::-1]
        sv = self.snake.snake_vec[::-1]
        for i, (s, v) in enumerate(zip(s, sv)):
            snake = None
            if i == 0:  # if the head
                snake = self.snake_tail_tile
                v = sv[i+1]  # keep the last orientation
            elif i == len(self.snake.snake) - 1:  # if the tail
                snake = self.snake_head_tile
            else:
                snake = self.snake_body_tile

            # Tile is up by default
            if v == Snake.DOWN:
                snake = pygame.transform.rotate(snake, 180)  # rotate down
            elif v == Snake.LEFT:
                snake = pygame.transform.rotate(snake, 90)  # rotate left
            elif v == Snake.RIGHT:
                snake = pygame.transform.rotate(snake, -90)  # rotate right

            self.display.blit(snake, (
                s[0] * self.tile_w,
                s[1] * self.tile_h
            ))
        # Print score
        score = self.font.render(
            self.score_str.format(self.snake.score),
            True,
            self.text_color
        )
        self.display.blit(score, (0, 0))

    def play_sound(self):
        """Play the right sound at the right moment during the game."""
        if self.snake.got_apple:
            self.gulp_sound.play()

    def play(self):
        """Start the game loop."""
        clock = pygame.time.Clock()
        vec = Snake.UP  # Initial direction of the snake
        self.loop_sound.play(loops=-1)
        while self.snake.is_alive:
            # Get input. Here is determined the `vec`.
            vec = self.get_input(vec)

            # Update the model
            self.snake.update(vec)

            # Play the sounds
            self.play_sound()

            # Display the state of the game
            self.print_display()
            pygame.display.update()

            clock.tick(15)  # FPS

        self.loop_sound.stop()
        self.game_over_sound.play()
        pygame.time.wait(3000)
