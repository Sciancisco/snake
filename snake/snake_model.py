# Copyright (c) 2019, Sciancisco
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyrigth holder nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""Python implementation of snake."""

from random import randint


class Snake:
    """Model of the game "snake"."""

    # Directions in wich the snake can go
    UP, DOWN, LEFT, RIGHT = (0, -1), (0, 1), (-1, 0), (1, 0)

    def __init__(self):
        """Instantiate a snake model."""
        self._xs = 30
        self._ys = 30
        self._snake = [
            (int(self._xs/2), int(self._ys/2)),
            (int(self._xs/2), int(self._ys/2+1)),
            (int(self._xs/2), int(self._ys/2+2))
        ]
        self._snake_vec = [  # Holds the orientation of each piece of snake
            self.UP,
            self.UP,
            self.UP
        ]
        while True:
            self._apple = (randint(0, self._xs-1), randint(0, self._ys-1))
            if self._apple not in self._snake:
                break

    def update(self, vec):
        """Update the state of the model."""
        if self.got_apple:
            self.grow(vec)
            self.spawn_apple()
        else:
            self.move(vec)

    def move(self, vec):
        """
        Move the snake in the specified direction.

        Direction can be `Snake.UP`, `Snake.DOWN`, `Snake.LEFT`, `Snake.RIGHT`.

        Must not be called at the same time as `grow()` or the snake will move
        twice.

        Parameters
        ----------
        vec: tuple or list
            the direction of the new head

        """
        del self._snake[-1]  # Delete last piece
        del self._snake_vec[-1]
        old_head = self._snake[0]
        new_head = (
            old_head[0] + vec[0],
            old_head[1] + vec[1]
        )
        self._snake.insert(0, new_head)  # Add new head in the right place
        self._snake_vec.insert(0, vec)

    def grow(self, vec):
        """
        Grow the snake in the specified direction.

        Direction can be `Snake.UP`, `Snake.DOWN`, `Snake.LEFT`, `Snake.RIGHT`.

        Must not be called at the same time as `move()` or the snake will move
        twice.

        Parameters
        ----------
        vec: tuple or list
            the direction of the new head

        """
        old_head = self._snake[0]
        new_head = (
            old_head[0] + vec[0],
            old_head[1] + vec[1]
        )
        self._snake.insert(0, new_head)  # Add a new head in the right place
        self._snake_vec.insert(0, vec)

    @property
    def snake(self):
        """Return a copy of the current snake."""
        return self._snake[0:]

    @property
    def snake_vec(self):
        """Return a copy of the vector of each piece of the snake."""
        return self._snake_vec[0:]

    @property
    def is_alive(self):
        """
        Ckeck if the snake is still alive.

        Return
        ------
        True if still alive, false otherwise

        """
        head = self._snake[0]

        # check out of map
        in_hor = head[0] >= 0 and head[0] < self._xs
        in_ver = head[1] >= 0 and head[1] < self._ys
        if not (in_hor and in_ver):
            return False

        # check itself
        if head in self._snake[1:]:
            return False

        return True

    def spawn_apple(self):
        """Spawn a new apple on the board."""
        while True:
            self._apple = (randint(0, self._xs-1), randint(0, self._ys-1))
            if self._apple not in self._snake:  # If apple is not in snake
                break

    @property
    def got_apple(self):
        """Check if the snake got the apple."""
        return self._snake[0] == self._apple

    @property
    def apple(self):
        """Return the position of the apple."""
        return self._apple

    @property
    def score(self):
        """Return the score of the current game."""
        return len(self._snake) - 3

    @property
    def xs(self):
        """Return the width of the board."""
        return self._xs

    @xs.setter
    def xs(self, value):
        """
        Set the width of the board.

        Parameters
        ----------
        value: int
            the new width of the board

        Raise
        -----
        ValueError:
            if `value` is not an int greater or equal to 4

        """
        v = abs(value)
        if v < 4 and type(v) == 'int':
            raise ValueError(
                "the witdth of the board must be an int greater or equal to 4."
            )
        self._xs = v

    @property
    def ys(self):
        """Return the height of the board."""
        return self._ys

    @ys.setter
    def ys(self, value):
        """
        Set the height of the board.

        Parameters
        ----------
        value: int
            the new width of the board

        Raise
        -----
        ValueError:
            if `value` is not an int greater or equal to 4

        """
        v = abs(value)
        if v < 4 and type(v) == 'int':
            raise ValueError(
                "the height of the board must be an int greater or equal to 4."
            )
        self._ys = v
