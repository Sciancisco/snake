# Snake

This repository contains an implementation of the game Snake in Python using the pygame library. Might be over engineered for a simple game... but hey!

## To run this program:
1. [Install Python](https://www.python.org/about/gettingstarted/). Tested with version 3.7.3, but should work with Python 3.x.
2. [Install pygame](https://www.pygame.org/wiki/GettingStarted). Tested with versions 1.9.4, 1.9.5 and 1.9.6, but should work with newer versions.
3. Run `main.py`. Command should look like `python main.py` (Windows) or `python3 main.py` (any other good \*nix OS).

## Customize
### Images
Every image used in the game is in the `assets/images` directory. You can change those images if you want but don't rename them and the game will pick them up. It is preferable that they are square and of same size. Project files for these images can be found in the `assets_raw` directory.

### Sounds
Every sound used in the game is in the `assets/sounds` directory. You can change those sounds but the same recommendations as for the image apply. The "game over" sound should be about 3 seconds or it will be cut by the game closing. Credits for those sounds can be found in the same directory.

## Known issue
Due to a bug with `pygame.mixer`, a core of your CPU may be pinned at 100%... To fix the behavior, raise a `pygame.error` in the try-except block that loads the sounds and the game will go on with placebos.

## Authors
code  : Sciancisco <3080877-Sciancisco@users.noreply.gitlab.com>
images: Sciancisco <3080877-Sciancisco@users.noreply.gitlab.com>
sounds: davidbain [freesound.org](https://freesound.org/people/davidbain/sounds/135831/), OwlStorm [freesound.org](https://freesound.org/people/OwlStorm/sounds/151234/), Volvion [freesound.org](https://freesound.org/people/Volvion/sounds/265308/)

v3.0.0

*Hope you enjoy :)*
